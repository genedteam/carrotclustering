package Courses;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by kevindiem on 1/29/17.
 */
public class CDictionary {

    private static final String[] strDict = new String[]{
            "grading", "grading policy", "plaiarism", "plagiarism policy", "attendance",
            "attendance policy", "policy", "quiz", "test",
            "hand in", "email", "turn in", "we", "will", "the", "class", "course", "students",
            "week", "university", "academic", "final", "late work", "late assignments",
            "work", "main campus", "readings", "this", "exam", "reading", "student", "grade",
            "blackboard", "may", "due", "can", "assignments", "paper", "you", "points", "use",
            "mwf", "mw", "wf", "mf", "tr", "spring", "fall", "crn", "temple","alter", "anderson",
            "andrsn", "beury", "cnwell", "glfltr", "gladfelter", "ritter", "ttlman", "tuttleman",
            "tyler", "wchman", "weiss", "hall", "general", "gened", "gen", "ed", "monday",
            "mondays", "tuesday", "tuesdays", "wednesday", "wednesdays", "thursday", "thursdays",
            "friday", "fridays", "saturday", "saturdays", "sunday", "sundays", "january", "jan",
            "february", "feb", "march", "mar", "april", "apr", "may", "june", "jun", "july", "jul",
            "august", "aug", "september", "sept", "october", "oct", "november", "nov", "december",
            "dec", "office hours", "pm", "am", "ampm", "appointment", "semester", "section",
            "department", "syllabus", "required texts", "contact", "information", "contact", "new",
            "do", "past due", "due", "tucc", "instructor", "instructors", "professor", "admin",
            "professors", "credit hours", "office", "assistant", "ta", "phd", "lectures", "lecture",
            "must", "by", "assignment", "chapter", "discussion", "one", "lab", "group", "please",
            "make", "read", "day", "questions", "attendance", "required", "another", "exams",
            "midterm", "midterms", "time", "two", "also", "plagiarism", "following", "material",
            "topic", "presentations", "taken", "project", "resources", "credit", "presentation",
            "based", "session", "sessions", "first", "introduction", "participation", "ideas",
            "part", "need", "assigned", "post", "posted", "note", "quizzes", "learn", "three",
            "discuss", "understand", "second", "different", "take", "review", "additional", "papers",
            "receive", "completed", "understanding", "present", "see", "specific", "cheating", "critical",
            "last", "video", "article", "concurrently", "requirement", "available", "well", "schedule",
            "without", "sources", "develop", "late", "teams", "expected", "materials", "subject", "pages",
            "next", "learning", "date", "topics", "including", "given", "weeks", "text", "possible", "chapters",
            "include", "faculty", "grades", "title", "classes", "site", "disability services", "using",
            "thinking", "meetings", "meeting", "disability policy", "honors", "honor", "enrolled", "ymay", "attributes",
            "repeated", "fulfills", "credit", "core", "credits", "succesfully", "ga", "gb", "gd", "gg", "gq", "gs", "gu",
            "gw", "gy", "gz"
    };

    public static Set<Object> filteredWords = Arrays.asList(strDict).stream()
            .collect(Collectors.toSet());
}
