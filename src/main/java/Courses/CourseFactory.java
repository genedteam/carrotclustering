package Courses;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.carrot2.core.Document;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kevindiem on 1/29/17.
 */
public class CourseFactory {

    private static final URL url = makeUrl();

    private CourseFactory(){}

    public static ArrayList<Document> getCourseDocs() throws Exception{

        CSVParser parser = getParser(url);

        Set<Document> courseSet = new HashSet<Document>();

        String[] courseNameNum;
        Document courseDoc;

        for(CSVRecord rec : parser){

            courseNameNum = rec.get(0).split(" ");

            if(courseNameNum[1].length() < 4){
                courseNameNum[1] = "0" + courseNameNum[1];
            }
            try {
                courseDoc = new Course(courseNameNum[0], courseNameNum[1]).toDocument();
                if(courseDoc != null)
                    courseSet.add(courseDoc);
            }catch(Exception e){
                System.out.println(e.toString());
            }
        }

        ArrayList<Document> courses = new ArrayList<Document>(courseSet);

        return courses;
    }



    private static URL makeUrl(){
        try {
            return new URL("http://astro.temple.edu/~tue96435/inv.csv");
        }catch(MalformedURLException e){
            return null;
        }
    }

    private static CSVParser getParser(URL url) throws IOException{
        return CSVParser.parse(url, Charset.defaultCharset(), CSVFormat.DEFAULT);
    }
}
