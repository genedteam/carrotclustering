package Courses;

/**
 * Created by kevindiem on 1/27/17.
 */


import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.util.EntityUtils;
import org.carrot2.core.Document;
import org.carrot2.core.LanguageCode;
import org.w3c.dom.NodeList;
import sun.misc.IOUtils;

import javax.xml.parsers.*;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;


public class Course {
    private String name;
    private String dept;
    private String num;
    private String description;

    private static final String SYLLABUS_DIR = "/Users/kevindiem/courseSearchOrg/R/text/";

    public Course(String dept, String num) throws  Exception{

        this.dept = dept;
        this.num = num;

        if(!downloadCourseInfo()) throw new Exception("Couldn't download course " + dept + " " + num);
    }

    public String getName() { return name; }

    public String getDescription(){ return description; }

    public Document toDocument() throws Exception{

        if(description == null) return null;

        return new Document(name, description, LanguageCode.ENGLISH);
    }

    private Boolean downloadCourseInfo() throws Exception {
        try {

            String url = "http://bulletin.temple.edu/ribbit?page=getcourse.rjs&code=" + dept + "%20" + num;

            CloseableHttpClient httpClient = HttpClientBuilder.create()
                    .setRedirectStrategy(new LaxRedirectStrategy())
                    .build();

            HttpGet httpGet = new HttpGet(url);

            HttpEntity entity = httpClient.execute(httpGet).getEntity();

            org.w3c.dom.Document doc = getXMLDocument(EntityUtils.toString(entity));

            this.description = parseDescription(doc);
            this.name = parseName(doc);

            return true;
        } catch(Exception e){

            e.printStackTrace();
            return false;
        }
    }

    private org.w3c.dom.Document getXMLDocument(String desc) throws Exception{

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        InputStream input = new ByteArrayInputStream(desc.getBytes("UTF-8"));

        org.w3c.dom.Document doc = builder.parse(input);

        return doc;
    }

    private String parseDescription(org.w3c.dom.Document doc) throws Exception {

        NodeList desc = getElementText(doc, "div");

        if(desc == null) return null;

        String parsedDesc = desc.item(1).getTextContent().replaceAll("\\n", "").replaceAll("NOTE: .*", "").replaceAll("Course Attributes: .*", "") + " " + parseSyllabus();

        //System.out.println(parsedDesc.replaceAll("\\n", "").replaceAll("NOTE: .*", "").replaceAll("Course Attributes: .*", ""));

        //System.out.println("");

        return parsedDesc;
    }

    private String parseSyllabus() throws  Exception{

        Scanner scanner = new Scanner( new File(SYLLABUS_DIR + dept + " " + num + ".txt") );
        String text = scanner.useDelimiter("\\A").next();
        scanner.close(); // Put this call in a finally block

        return filterSyllabus(text.toLowerCase());

    }

    private String parseName(org.w3c.dom.Document doc) throws  Exception{

        String regex = dept + "|\\d|(Credit Hours)|\\.|\\n.+";

        NodeList desc = getElementText(doc, "p");

        if(desc == null) return null;

        String parsedName = desc.item(0).getTextContent();

        return parsedName.replaceAll(regex, "");
    }

    private NodeList getElementText(org.w3c.dom.Document doc, String htmlTag){
        try {

            NodeList course = doc.getElementsByTagName("course");

            String htmlContent = course.item(0).getTextContent();

            org.w3c.dom.Document htmlDoc = getXMLDocument(htmlContent);

            NodeList desc = htmlDoc.getElementsByTagName(htmlTag);

            return desc;
        }catch(Exception e){
            return null;
        }
    }

    private String filterSyllabus(String description){


        String[] desc = description.toLowerCase()
                .replaceAll("\\d|("+ name +")|(human behavior)|(analytical reading & writing)|(quantitative literacy)|(mosaic)|(arts)|(race and diversity)|(world society)|(science & technology)|(u.s. society)", "")
                .split(" ");


        Set<Object> descSet = Arrays.asList(desc).stream()
                .collect(Collectors.toSet());

        descSet.removeAll(CDictionary.filteredWords);

        return StringUtils.join(descSet, " ");
    }


}
