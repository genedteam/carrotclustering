/**
 * Created by kevindiem on 1/27/17.
 */

import CarrotClusters.ConsoleFormatter;
import Courses.CourseFactory;
import org.carrot2.clustering.lingo.LingoClusteringAlgorithm;
import org.carrot2.core.*;


import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception{


        ArrayList<Document> courseDocs = CourseFactory.getCourseDocs();

        final Controller controller = ControllerFactory.createSimple();

        /*
         * Perform clustering by topic using the Lingo algorithm. Lingo can
         * take advantage of the original query, so we provide it along with the documents.
         */
        final ProcessingResult byTopicClusters = controller.process(courseDocs, null,
                LingoClusteringAlgorithm.class);

        final List<Cluster> clustersByTopic = byTopicClusters.getClusters();


        ConsoleFormatter.displayClusters(clustersByTopic);

    }
}
