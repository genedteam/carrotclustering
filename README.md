# Carrot Clustering #

This app utilizes Carrot to perform a cluster analysis of GenEd courses along with their descriptions. In order to do this it reads a CSV then scrapes and parses descriptions from Temple University's course API, course leaf. 

### How do I get set up? ###

* Install Java
* Clone the repo
* Ensure the inventory CSV is available at it's current location (or download it from [https://bitbucket.org/genedteam/gened-course-data]) 
* Install the maven dependencies
* Run the app


MIT License